package com.springApp.coursesApi.courses;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springApp.coursesApi.topics.Topic;

@Service
public class CourseService {
	
	@Autowired
	private CoursesRepository repCourses;
	
	public List<Course> getCourses(String topicId){
		List<Course> courses = new ArrayList<>();
		repCourses.findByTopicId(topicId)
		.forEach(courses::add);
		return courses;
	}
	
	public Course getCourse(String courseId){
		return 	repCourses.findById(courseId).get();
	}

	public Course addCourse(String topicId, Course course) {
		course.setTopic(new Topic(topicId, "", ""));
		repCourses.save(course);
		return course;
	}

	public void updateCourse(Course course, String courseId) {
		/*repCourses.findByTopicId(topicId);*/
		repCourses.save(course);
	}

	public void deleteCourse(String courseId) {
		/*repCourses.findByTopicId(topicId);*/
		repCourses.deleteById(courseId);
	}
}

package com.springApp.coursesApi.topics;

import org.springframework.data.repository.CrudRepository;

public interface TopicsRepository extends CrudRepository<Topic, String> {

}

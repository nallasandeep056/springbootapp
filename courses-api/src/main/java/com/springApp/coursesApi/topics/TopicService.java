package com.springApp.coursesApi.topics;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopicService {
	
	@Autowired
	private TopicsRepository repTopics;
	
	public List<Topic> getTopics(){
		List<Topic> topics = new ArrayList<>();
		repTopics.findAll()
		.forEach(topics::add);
		return topics;
	}
	
	public Topic getTopic(String id){
		return repTopics.findById(id).get();
	}

	public void addTopic(Topic topic) {
		repTopics.save(topic);
	}

	public void updateTopic(Topic topic, String id) {
		repTopics.save(topic);
	}

	public void deleteTopic(String id) {
		repTopics.deleteById(id);
	}
	
}
